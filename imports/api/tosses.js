import {Mongo} from 'meteor/mongo';

if (Meteor.isServer) {
    Meteor.publish('tosses', function tossesPublication(conditions, params) {
        params = params || {};
        conditions = conditions || {};
        conditions.owner = Meteor.userId();
        params.limit = 500;

        return Tosses.find(conditions, params);
    });
}

export const Tosses = new Mongo.Collection('tosses');